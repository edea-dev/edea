API reference
==============
.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   kicad/index
   metadata
   cli

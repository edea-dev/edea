Checker
=======

Checker interface
------------------

.. automodule:: edea.kicad.checker
   :members:
   :undoc-members:

Design rules checker
--------------------
.. automodule:: edea.kicad.checker.drc
   :members:
   :undoc-members:
   :show-inheritance:

Electrical rules checker
-------------------------
.. automodule:: edea.kicad.checker.erc
   :members:
   :undoc-members:
   :show-inheritance:


Reporter
--------
.. automodule:: edea.kicad.checker.reporter
   :members:
   :undoc-members:
   :show-inheritance:

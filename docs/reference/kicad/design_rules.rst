Design rules
=============

.. automodule:: edea.kicad.design_rules
   :members:
   :undoc-members:
   :show-inheritance:

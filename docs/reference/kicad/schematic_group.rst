Schematic group
================

.. automodule:: edea.kicad.schematic_group
   :members:
   :undoc-members:
   :show-inheritance:

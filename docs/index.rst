EDeA tool documentation
=======================

.. toctree::
   :maxdepth: 3
   :caption: Content:
   
   getting_started
   cli
   reference/index
   changelog
   license
   dev

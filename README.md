# edea python library

The core python library and command line interface for the EDeA project.

## Documentation
[latest docs](https://edea-dev.gitlab.io/edea/latest/)

## Getting started
[Getting started](https://edea-dev.gitlab.io/edea/latest/getting_started.html)


## CHANGELOG
[CHANGELOG](https://edea-dev.gitlab.io/edea/latest/changelog.html)

## Developer documentation
[Developer docs](https://edea-dev.gitlab.io/edea/latest/dev.html)